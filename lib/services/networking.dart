import 'dart:convert';

import 'package:http/http.dart' as http;

class NetworkingHelper {
  NetworkingHelper({this.url});

  final String url;

  Future getData() async {
    http.Response response = await http.get(url);
    int statusCode = response.statusCode;
    if (statusCode == 200) {
      String data = response.body;
      return jsonDecode(data);
    } else {
      print('Status code: $statusCode');
      return null;
    }
  }
}
