import 'package:clima/services/location.dart';
import 'package:clima/services/networking.dart';

import 'networking.dart';

const String _apiKey = '3a9c291f86dd7543fca021e66cd9376d';
const String _openWeatherURL =
    'https://api.openweathermap.org/data/2.5/weather';

class WeatherModel {
  Future<dynamic> getWeatherForCity(String cityName) async {
    String url = '$_openWeatherURL?q=$cityName&appid=$_apiKey&units=metric';
    NetworkingHelper helper = NetworkingHelper(url: url);
    var result = await helper.getData();
    return result;
  }

  Future<dynamic> getCurrentLocationData() async {
    Location location = Location();
    await location.getCurrentLocation();
    double latitude = location.getLatitude();
    double longitude = location.getLongitude();
    String url =
        '$_openWeatherURL?lat=$latitude&lon=$longitude&appid=$_apiKey&units=metric';
    NetworkingHelper networkingHelper = NetworkingHelper(url: url);
    var result = await networkingHelper.getData();
    return result;
  }

  String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
